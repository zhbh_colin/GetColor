package com.dui.getcolor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ColorImageView mColorImageView;
    private TextView mTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //找到控件
        mColorImageView = (ColorImageView) findViewById(R.id.iv_color_palette);
        mTextView = (TextView) findViewById(R.id.color_test);
        //设置监听
        mColorImageView.setOnColorSelectedListener(new ColorImageView.OnColorSelectedListener() {
            @Override
            public void onColorSelectedL(int color) {
                mTextView.setBackgroundColor(color);
            }
        });
    }
}